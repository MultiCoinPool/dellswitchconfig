package DellSwitch

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"io"
	"log"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

var Ports = make(map[string]*Port)
var Vlans = make(map[int]*VLANS)
var switchChanIn = make(chan string, 5)
var switchChanOut = make(chan string, 5)

func ClientInit(ip, user, pass string) {
	config := &ssh.ClientConfig{
		User: user,
		BannerCallback: func(message string) error {
			log.Printf("Banner: %s", message)
			return nil
		},
		Auth: []ssh.AuthMethod{
			ssh.Password(pass),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         30 * time.Second,
	}
	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:22", ip), config)
	if err != nil {
		log.Fatal("Failed to dial: ", err)
	}

	// Each ClientConn can support multiple interactive sessions,
	// represented by a Session.
	session, err := client.NewSession()
	if err != nil {
		log.Fatal("Failed to create session: ", err)
	}
	//defer session.Close()
	modes := ssh.TerminalModes{
		ssh.ECHO:          0,     // disable echoing
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	}
	// Request pseudo terminal
	if err := session.RequestPty("vt100", 40, 80, modes); err != nil {
		log.Fatal("request for pseudo terminal failed: ", err)
	}

	w, err := session.StdinPipe()
	if err != nil {
		panic(err)
	}
	r, err := session.StdoutPipe()
	if err != nil {
		panic(err)
	}
	e, err := session.StderrPipe()
	if err != nil {
		panic(err)
	}

	MuxShell(w, r, e)
	if err := session.Shell(); err != nil {
		log.Fatal(err)
	}

	<-switchChanOut //ignore the shell output
	switchChanIn <- "enable"
	<-switchChanOut

	GetVlans()
	//log.Printf(spew.Sdump(Vlans))
}

func GetPortInfo(port string) *PortVlan {
	iType := "ethernet"
	realPort := port
	if strings.HasPrefix(port, "Gi") {
		iType = "Gigabitethernet"
		realPort = strings.Replace(port, "Gi", "", 1)
	}
	switchChanIn <- fmt.Sprintf("show interfaces switchport %s %s", iType, realPort)
	return parsePortParameters(<-switchChanOut)
}

func AddVlanPort(port string, vlan int, tagged bool, pvid bool) {
	iType := "ethernet"
	realPort := port
	if strings.HasPrefix(port, "Gi") {
		iType = "Gigabitethernet"
		realPort = strings.Replace(port, "Gi", "", 1)
	}

	switchChanIn <- "configure"
	<-switchChanOut
	switchChanIn <- fmt.Sprintf("interface %s %s", iType, realPort)
	<-switchChanOut
	switchChanIn <- "switchport mode general"
	<-switchChanOut
	tag := "untagged"
	if tagged {
		tag = "tagged"
	}
	switchChanIn <- fmt.Sprintf("switchport general allowed vlan add %d %s", vlan, tag)
	<-switchChanOut
	if pvid {
		switchChanIn <- fmt.Sprintf("switchport general pvid %d", vlan)
		<-switchChanOut
	}
	switchChanIn <- "exit"
	<-switchChanOut
	switchChanIn <- "exit"
	<-switchChanOut
	Vlans = make(map[int]*VLANS)
	GetVlans()
}

func DeleteVlanPort(port string, vlan int) {
	iType := "ethernet"
	realPort := port
	if strings.HasPrefix(port, "Gi") {
		iType = "Gigabitethernet"
		realPort = strings.Replace(port, "Gi", "", 1)
	}

	switchChanIn <- "configure"
	<-switchChanOut
	switchChanIn <- fmt.Sprintf("interface %s %s", iType, realPort)
	<-switchChanOut
	switchChanIn <- "switchport mode general"
	<-switchChanOut
	switchChanIn <- fmt.Sprintf("switchport general allowed vlan remove %d", vlan)
	<-switchChanOut
	if Ports[port].PortVlan.Pvid == vlan {
		switchChanIn <- "switchport general pvid 1"
		<-switchChanOut
	}
	switchChanIn <- "exit"
	<-switchChanOut
	switchChanIn <- "exit"
	<-switchChanOut
	Vlans = make(map[int]*VLANS)
	GetVlans()
}

func AddVlan(numb int) {
	log.Printf("Adding vlan: %d", numb)
	switchChanIn <- "configure"
	<-switchChanOut
	switchChanIn <- "vlan database"
	<-switchChanOut
	switchChanIn <- fmt.Sprintf("vlan %d", numb)
	<-switchChanOut
	switchChanIn <- "exit"
	<-switchChanOut
	switchChanIn <- "exit"
	<-switchChanOut
	Vlans = make(map[int]*VLANS)
	GetVlans()
}

func DeleteVlan(numb int) {
	switchChanIn <- "configure"
	<-switchChanOut
	switchChanIn <- "vlan database"
	<-switchChanOut
	switchChanIn <- fmt.Sprintf("no vlan %d", numb)
	<-switchChanOut
	switchChanIn <- "exit"
	<-switchChanOut
	switchChanIn <- "exit"
	<-switchChanOut
	Vlans = make(map[int]*VLANS)
	GetVlans()
}

func GetPorts() map[string]*Port {
	if len(Ports) == 0 {
		switchChanIn <- "show interfaces status"
		interfacestable := parseOutput(<-switchChanOut)
		for _, v := range interfacestable {
			p := &Port{}
			p.Port = v[0]
			p.Type = v[1]
			p.Duplex = v[2]
			p.Speed = v[3]
			p.Negotiation = v[4]
			p.State = v[5]
			p.FlowControl = v[6]

			p.PortVlan = GetPortInfo(p.Port)

			Ports[p.Port] = p
		}
	}
	return Ports
}

func GetVlans() map[int]*VLANS {
	if len(Vlans) == 0 {
		if len(Ports) == 0 {
			GetPorts()
		}
		switchChanIn <- "show vlan"
		vlanstable := parseOutput(<-switchChanOut)
		for _, v := range vlanstable {
			vid, err := strconv.Atoi(v[0])
			if err != nil {
				log.Fatalf("Unable to int convert vlan id: %s", err)
			}
			vlan := &VLANS{}
			vlan.Id = vid
			vlan.Name = v[1]
			vlan.Ports = make([]*Port, 0)
			vlan.Type = v[3]
			//vlan.Authorization = v[4]

			vlanportsrange := strings.Split(v[2], ",")
			for _, v2 := range vlanportsrange {
				if v2 == "" {
					continue // Nothing to see here; Move along
				}
				if strings.Contains(v2, "-") { // Range is defined
					// TODO: Dont know how stacking ports react, so I'm ignoring it for now.
					dashIndex := strings.Index(v2, "-")
					starting := v2[:dashIndex]
					ending := v2[dashIndex+1:]

					// just for check... Because it looks like channels (ch) dont use a ch on the ending....... dumb
					if _, err := strconv.ParseInt(ending, 10, 64); err == nil {
						ending = "ch" + ending
					}
					log.Printf("Starting: %s | Ending port#: %s", starting, ending)

					stndrd := regexp.MustCompile(`(\d)/(.*?)(\d+)`)
					chgroup := regexp.MustCompile(`ch(\d+)`)
					hits := stndrd.FindStringSubmatch(starting)
					portTypeStart := ""
					portNumberStart := 0
					if len(hits) != 0 { // Matched a standard port looking thing
						portTypeStart = hits[2]
						portNumberStart, _ = strconv.Atoi(hits[3])
					} else {
						// Try to match a non-standard channel number
						hits = chgroup.FindStringSubmatch(starting)
						if len(hits) != 0 {
							portTypeStart = "ch"
							portNumberStart, _ = strconv.Atoi(hits[1])
						} else {
							log.Printf("Non-standard port set: %s", v2)
						}
					}
					hits = stndrd.FindStringSubmatch(ending)
					portTypeEnding := ""
					portNumberEnding := 0
					if len(hits) != 0 { // Matched a standard port looking thing
						portTypeEnding = hits[2]
						portNumberEnding, _ = strconv.Atoi(hits[3])
					} else {
						// Try to match a non-standard channel number
						hits = chgroup.FindStringSubmatch(ending)
						if len(hits) != 0 {
							portTypeEnding = "ch"
							portNumberEnding, _ = strconv.Atoi(hits[1])
						} else {
							log.Printf("Non-standard port set: %s", v2)
						}
					}

					if portTypeStart != portTypeEnding {
						// Going from 1G ports to 10G ports
						for k := portNumberStart; k <= 48; k++ { // TODO: hardcode a max?
							if p, ok := Ports[fmt.Sprintf("1/%s%d", portTypeStart, k)]; ok {
								vlan.Ports = append(vlan.Ports, p)
							}
						}
						for k := 1; k <= portNumberEnding; k++ { // TODO: hardcode a max?
							if p, ok := Ports[fmt.Sprintf("1/%s%d", portTypeEnding, k)]; ok {
								vlan.Ports = append(vlan.Ports, p)
							}
						}

					} else {
						for k := portNumberStart; k <= portNumberEnding; k++ {
							if p, ok := Ports[fmt.Sprintf("1/%s%d", portTypeStart, k)]; ok {
								vlan.Ports = append(vlan.Ports, p)
							} else {
								log.Printf("Port in range is not in the port list. %d | %s", k, v2)
							}
						}
					}

				} else {
					if p, ok := Ports[v2]; ok {
						vlan.Ports = append(vlan.Ports, p)
					} else {
						log.Printf("Single port defined in vlan, not actual known port: %s", v2)
					}
				}
			}

			Vlans[vlan.Id] = vlan
		}
	}
	return Vlans
}

type VLANS struct {
	Id    int
	Name  string
	Ports []*Port
	Type  string
	//Authorization string
}

type Port struct {
	Port        string
	Type        string
	Duplex      string
	Speed       string
	Negotiation string
	State       string
	FlowControl string
	PortVlan    *PortVlan
}

type PortVlan struct {
	Port      string
	Mode      string
	Pvid      int
	InFilter  bool
	FrameType string
	Vlans     []*PortVlanMember
}

type PortVlanMember struct {
	Vlan   int
	Name   string
	Egress string
	Type   string
}

func parsePortParameters(in string) *PortVlan {
	ret := make(map[string]string)
	paramsSplit := strings.Split(in, "\r\n")
	//log.Printf(spew.Sdump(paramsSplit))
	sep := regexp.MustCompile(`(.*?):(.*)`)
	tableIndex := 0
	for i, v := range paramsSplit { // TODO: Use the index to count to where the table sohuld be
		// Since the table will be after the same amount of parameter rows every time
		// Independent of port type
		if v == "" {
			if len(ret) == 0 {
				continue
			}
			break
		}
		//log.Printf("index: %d | %s", i, v)
		fsi := sep.FindStringSubmatch(v)
		if len(fsi) == 3 {
			//log.Printf(spew.Sdump(fsi))
			if len(fsi[2]) == 0 && i > 4 {
				//log.Printf("Have table maybe?")
				tableIndex = i
				break
				//continue // Might be start of a new table/params
				// TODO: How to get the next few lines to parse the tables?
			}
			if len(fsi[2]) != 0 {
				ret[fsi[1]] = strings.TrimSpace(fsi[2])
			}
		}
	}

	usableTable := strings.Join(paramsSplit[tableIndex:], "\r\n")
	table := parseOutput(usableTable)

	p := &PortVlan{}
	p.Port = ret["Port"]
	p.FrameType = ret["Acceptable Frame Type"]
	p.InFilter = false
	if ret["Ingress Filtering"] == "Enabled" {
		p.InFilter = true
	}
	p.Mode = ret["VLAN Membership mode"]
	p.Pvid, _ = strconv.Atoi(ret["PVID"])
	p.Vlans = make([]*PortVlanMember, 0)
	for _, v := range table {
		pv := &PortVlanMember{}
		pv.Vlan, _ = strconv.Atoi(v[0])
		pv.Name = v[1]
		pv.Egress = v[2]
		pv.Type = v[3]
		p.Vlans = append(p.Vlans, pv)
	}

	return p
}

func parseOutput(in string) [][]string {
	table := make([][]string, 0)
	sep := regexp.MustCompile(`\s-`)
	colSizes := make([]int, 0)
	vlanSplit := strings.Split(in, "\r\n")
	for _, v := range vlanSplit {
		if v == "" {
			if len(table) == 0 {
				continue
			} else {
				break
			}
		}
		if strings.HasPrefix(v, "-") {
			//log.Printf("Start col width counts. %d", i)
			fsi := sep.FindAllStringIndex(v, -1)
			//log.Printf("finding: %+v", fsi)

			useIndex := 0
			for _, v2 := range fsi {
				col := v[useIndex : v2[1]-1]
				useIndex = v2[1] - 1
				//log.Printf("col picked out: '%s' | %d", col, len(col))
				colSizes = append(colSizes, len(col))
			}
			colSizes = append(colSizes, len(v[useIndex:]))
			//log.Printf("Last col picked: '%s' | %d", v[useIndex:], len(v[useIndex:]))
		} else if len(colSizes) != 0 {
			if v == "" {
				break
			}
			start := 0

			row := make([]string, len(colSizes))
			for i2, v2 := range colSizes {
				lastIndex := start + v2
				colVal := v[start:]
				spaceIndex := strings.Index(colVal, " ")
				//log.Printf("whole: '%s' | space: %d | last: %d | start: %d", colVal, spaceIndex, lastIndex, start)
				if spaceIndex+start >= lastIndex {
					colVal = v[start : spaceIndex+start]
				} else if lastIndex < len(v) {
					colVal = v[start:lastIndex]
				}
				row[i2] = strings.TrimSpace(colVal)
				//log.Printf("Value pulled for %d (%d s: %d): '%s'", i2, v2, start, colVal)
				start += len(colVal)
			}

			if row[0] == "" { // We know this should be part of the previous row...
				prev := table[len(table)-1]
				new := make([]string, len(prev))
				for i2, v2 := range prev {
					new[i2] = v2 + row[i2]
				}
				table[len(table)-1] = new
			} else {
				table = append(table, row)
			}
		}
	}
	return table
}

func MuxShell(w io.Writer, r, e io.Reader) {
	var wg sync.WaitGroup
	wg.Add(1) //for the shell itself
	go func() {
		for cmd := range switchChanIn {
			wg.Add(1)
			w.Write([]byte(cmd + "\n"))
			wg.Wait()
		}
	}()

	go func() {
		var (
			buf [1024 * 1024]byte
			t   int
		)
		for {
			n, err := r.Read(buf[t:])
			if err != nil {
				fmt.Println(err.Error())
				//close(switchChanIn)
				//close(switchChanOut)
				return
			}
			t += n
			result := string(buf[:t])
			if strings.Contains(string(buf[t-n:t]), "More") {
				//log.Printf("Out More: %s", string(buf[t-n:t]))
				w.Write([]byte("\n"))
			}
			if strings.Contains(result, "username:") ||
				strings.Contains(result, "password:") ||
				strings.HasSuffix(result, "#") ||
				strings.Contains(result, ">") {
				switchChanOut <- strings.ReplaceAll(string(buf[:t]), "--More-- or (q)uit\r", "")
				t = 0
				wg.Done()
			}
		}
	}()
	return
}
